package ants;

import core.AntColony;
import core.Bee;

/**
 * An ant who throws leaves at bees far away.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class LongThrowerAnt extends ThrowerAnt {

    public LongThrowerAnt() {
        super(3, 1);
    }

    @Override
    public Bee getTarget () {
        return place.getClosestBee(4, AntColony.MAX_TUNNEL_LENGTH);
    }
}
