package ants;

import core.Ant;
import core.AntColony;

/**
 * The Wall ant can block bees until he is alive.
 * The Wall ant has 4 points of armor.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class WallAnt extends Ant {

	/**
	 * Default constructor.
	 */
	public WallAnt(){
		super(4, 4);
	}

	@Override
	/**
	 * Do nothing, just suffer damages
	 */
	public void action(AntColony colony) {
	}

}
