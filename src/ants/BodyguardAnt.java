package ants;

import core.Ant;
import core.AntColony;
import core.Containing;
import core.Insect;

/**
 * The BodyGuardAnt aims at protecting others ants.
 * BodyGuardAnt receives bees' damages for the protected ant.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class BodyguardAnt extends Ant implements Containing {
	
	private Insect containedInsect = null;

	/**
	 * Default constructor.
	 */
	public BodyguardAnt() {
		super(5, 2);
	}

	@Override
	public boolean addContainedInsect(Insect insect) {
		if(containedInsect == null){
			containedInsect = insect;
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean removeContainedInsect() {
		if(containedInsect != null){
			containedInsect = null;
			return true;
		}
		else
			return false;
	}

	@Override
	public Insect getContainedInsect() {
		return containedInsect;
	}

	@Override
	public void action(AntColony colony) {
		/* Execute 2 fois l'action de l'insecte contenu sinon et c'est caca
		if(containedInsect != null)
			containedInsect.action(colony);
		*/
	}

}
