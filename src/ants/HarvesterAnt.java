package ants;

import core.Ant;
import core.AntColony;

/**
 * An Ant that harvests food for the colony.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class HarvesterAnt extends Ant {

	/**
	 * Creates a new Harvester Ant
	 */
	public HarvesterAnt () {
		super(2, 1);
	}

	@Override
	public void action (AntColony colony) {
		// Harvests food
		colony.increaseFood(1);
	}
}
