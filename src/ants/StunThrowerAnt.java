package ants;

import core.AntColony;
import core.Bee;

/**
 * An ant who throws leaves at bees and stuns bees during 1 tour.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class StunThrowerAnt extends ThrowerAnt {

    public StunThrowerAnt() {
        super(6, 1);
    }

    @Override
    public void action (AntColony colony) {
        Bee target = getTarget();
        if (target != null) {
            target.stun(1);
        }
    }

}
