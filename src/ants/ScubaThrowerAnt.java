package ants;

/**
 * Ants are smart, they have created a diving suit to go under water!
 * ScubaThrowerAnt Is the first special ant with this feature.
 * She can throw leaves as the Thrower ant.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class ScubaThrowerAnt extends ThrowerAnt {

	/**
	 * Default constructor.
	 */
	public ScubaThrowerAnt() {
		super(5, 1);
		watersafe = true;
	}

	public ScubaThrowerAnt (int foodCost, int armor) {
		super(foodCost, armor);
		watersafe = true;
	}

}
