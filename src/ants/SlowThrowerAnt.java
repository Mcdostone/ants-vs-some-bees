package ants;

import core.AntColony;
import core.Bee;

/**
 * An ant who throws leaves at bees and slows them
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class SlowThrowerAnt extends ThrowerAnt {

    public SlowThrowerAnt() {
        super(4, 1);
    }

    @Override
    public void action (AntColony colony) {
        Bee target = getTarget();
        if (target != null) {
            target.stun(2);
        }
    }

}
