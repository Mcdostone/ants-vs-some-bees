package ants;

import core.Ant;
import core.AntColony;
import core.Damaging;
import core.QueenPlace;
import core.Place;


/**
 * The QueenAnt is the most majestic ant of the colony.
 * She's not afraid of water.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class QueenAnt extends ScubaThrowerAnt {
	
	private Ant[] galvanisedAnts;

    // Because of initialisation of the game.
	private static int instances = -1;

    private boolean imposteur = false;

    /**
     * Default constructor
     */
	public QueenAnt() {
		super(6, 1);
		this.galvanisedAnts = new Ant[2];

		QueenAnt.instances += 1;

        if(QueenAnt.instances > 1)
            this.imposteur = true;
	}

	@Override
	public void action (AntColony colony) {
        if(this.imposteur) {
            colony.increaseFood(this.foodCost);
            this.reduceArmor(this.getDamage());
        }

        else {
        	if(this.place.getEntrance() != null)
				checkGalvanise(this.place.getEntrance(), 0);
        	if(this.place.getExit() != null)
        		checkGalvanise(this.place.getExit(), 1);
            ((QueenPlace) colony.getQueenPlace()).setRealQueenPlace(this.place);

            super.action(colony);
        }
    }

    private void checkGalvanise(Place p, int galvanisedAntIndex){
        if(p.getAnt() != null && p.getAnt() != galvanisedAnts[galvanisedAntIndex]){
            if(galvanisedAnts[galvanisedAntIndex] != null && galvanisedAnts[galvanisedAntIndex] instanceof Damaging)
                ((Damaging) galvanisedAnts[galvanisedAntIndex]).galvanise(false);
            if(p.getAnt() instanceof Damaging){
                ((Damaging) p.getAnt()).galvanise(true);
                galvanisedAnts[galvanisedAntIndex] = p.getAnt();
            }
        }
    }

    public void reduceArmor (int amount) {
        super.reduceArmor(amount);
        if(this.armor <= 0)
            QueenAnt.instances--;
    }
}
