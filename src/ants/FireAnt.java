package ants;

import core.AntColony;
import core.Bee;

/**
 * A fire Ant is an ant who deals damages to bees (who are at the same place) when she dies.
 * A fire ant costs 4 food and has 1 point of armor.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class FireAnt extends DamagingAnt {

	/**
	 * Default constructor.
	 */
	public FireAnt(){
		super(4, 1);
		// Damage that she deals to bees
		this.damage = 3;
	}


	@Override
	public 	void action(AntColony colony) {}

	@Override
	/**
	 * Override the reduceArmor method in order to change
	 * the behavior when this ant dies.
	 */
	public void reduceArmor(int amount){
		if(this.getArmor() - amount <= 0){
			Bee[] bees = this.place.getBees();
			for(Bee b : bees){
				b.reduceArmor(this.getDamage());
			}
		}
		super.reduceArmor(amount);
	}

}
