package ants;

import core.AntColony;
import core.Bee;

/**
 * An ant who throws leaves at bees.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class ThrowerAnt extends DamagingAnt {


	/**
	 * Creates a new Thrower Ant.
	 */
	public ThrowerAnt () {
		super(4, 1);
		this.damage = 1;
	}

	/**
	 * Second constructor.
	 *
	 * @param foodCost Amount of food needed to make this ant.
	 * @param armor Armor of this ant.
     */
	public ThrowerAnt (int foodCost, int armor) {
		super(foodCost, armor);
	}

	/**
	 * @return A bee to target
	 */
	public Bee getTarget () {
		return place.getClosestBee(0, 3);
	}

	@Override
	public void action (AntColony colony) {
		Bee target = getTarget();
		if (target != null) {
			target.reduceArmor(this.getDamage());
		}
	}

}
