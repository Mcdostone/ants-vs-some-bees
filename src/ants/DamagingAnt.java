package ants;

import core.Ant;
import core.AntColony;
import core.Damaging;

/**
 * Ant which deals damages to bees.
 * Every ant which deals damage must inherits from this class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public abstract class DamagingAnt extends Ant implements Damaging {

    protected int damage;
    protected boolean galvanised;

    public DamagingAnt(int armor) {
        super(armor);
        this.init();
    }

    public DamagingAnt (int foodCost, int armor) {
        super(foodCost, armor);
        this.init();
    }

    /**
     * Init attributes
     */
    private void init() {
        this.damage = 1;
        this.galvanised = false;
    }

    @Override
    public void galvanise(boolean b) {
        this.galvanised = b;
    }

    @Override
    public int getDamage() {
        return (this.galvanised) ? this.damage * 2 : this.damage;
    }
}
