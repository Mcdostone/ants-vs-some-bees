package ants;

import core.AntColony;
import core.Bee;

/**
 * NinjaAnt is so discreet that bees can't see her.
 * What's more, the NinjaAnt attacks all bees that fly over its place.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class NinjaAnt extends DamagingAnt {

    /**
     * Default constructor.
     */
	public NinjaAnt() {
		super(6, 1);
		blockingBee = false;
		this.damage = 1;
	}

	@Override
	public void action (AntColony colony) {
		Bee[] nearBees = place.getBees();
		for(Bee curBee : nearBees){
			curBee.reduceArmor(this.getDamage());
		}
	}

}
