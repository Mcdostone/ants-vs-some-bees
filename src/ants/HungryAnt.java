package ants;

import core.Ant;
import core.AntColony;

/**
 * The hungry ant is so hungry that she will devour the first bee on her place.
 * But be careful, this ant has only 1 point of armor and there
 * is a cooldown for repeating this main action.
 *
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class HungryAnt extends Ant {

	// Count number of time.
	private int timer = 0;
	// Cooldown of action
	private final int cooldown = 3;

	/**
	 * Default constructor.
	 */
	public HungryAnt(){
		super(4, 1);
	}

	/**
	 * Kill instantaneously the bee on the current place.
	 */
	private void eatBee(){
		this.place.getBees()[0].reduceArmor(this.place.getBees()[0].getArmor());
		timer = cooldown;
	}

	@Override
	public void action(AntColony colony) {
		if(timer <= 0 && this.place.getBees().length > 0)
            eatBee();
		else
			timer--;
	}

}
