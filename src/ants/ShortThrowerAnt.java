package ants;

import core.Bee;

/**
 * An ant who throws leaves at bees in short distance.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class ShortThrowerAnt extends ThrowerAnt {

    public ShortThrowerAnt() {
        super(3, 1);
    }

    @Override
    public Bee getTarget () {
        return place.getClosestBee(0, 2);
    }

}
