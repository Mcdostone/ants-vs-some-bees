package bees;

import core.Bee;

/**
 * More powerful bee
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class StrongBee extends Bee {

    public StrongBee() {
        super(5);
    }

}
