package tests;

import ants.QueenAnt;

import static org.junit.Assert.assertTrue;

/**
 * Test class for QueenAnt class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class TestQueenAnt extends TestAnt {

    public TestQueenAnt() {
        super(new QueenAnt(), 6, 1);
    }


    @Override
    public void testAction() {

    }

    @Override
    public void testWatersafe() {
        assertTrue("The " + this.cobaye.getClass().getName() + " should be watersafe", this.cobaye.isWatersafe());
    }

}
