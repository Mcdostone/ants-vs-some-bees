package tests;

import ants.ShortThrowerAnt;
import core.AntColony;
import core.Bee;

import static org.junit.Assert.assertEquals;

/**
 * Test class for ShortThrowerAnt class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class TestShortThrowerAnt extends TestAnt {

    private int mayaArmor = 3;
    private Bee maya;


    public TestShortThrowerAnt() {
        super(new ShortThrowerAnt(), 3, 1);
    }


    @org.junit.Before
    /**
     * This method is called before each test method.
     */
    public void setUp() throws Exception {
        super.setUp();
        this.colony =  new AntColony(1, 5, 0, 3);
        this.maya = new Bee(this.mayaArmor);
    }


    @org.junit.Test
    /** Test action method in a normal case */
    public void testAction() {
        int armorBefore = this.maya.getArmor();

        placeAnt(this.cobaye, 0);
        // Distance : 2
        placeBee(this.maya, 2);

        this.cobaye.action(this.colony);
        this.maya.action(this.colony);

        // 3 - foodCost = 0
        assertEquals("The colony should have 0 food", 0, this.colony.getFood());
        assertEquals("Maya should lost 1 point of armor", armorBefore - 1, this.maya.getArmor());
    }



    @org.junit.Test
    /** Test action method when the bee is not in the range of the TestThrowerAnt */
    public void testActionBeeNotInRange() {
        int armorBefore = this.maya.getArmor();

        placeAnt(this.cobaye, 0);
        placeBee(this.maya, 3);

        this.cobaye.action(this.colony);
        this.maya.action(this.colony);

        // 3 - foodCost = 0
        assertEquals("The colony should have 0 food", 0, this.colony.getFood());
        assertEquals("Maya shouldn't be touched", armorBefore, this.maya.getArmor());

        this.cobaye.action(this.colony);
        this.maya.action(this.colony);

        assertEquals("The colony should have 0 food", 0, this.colony.getFood());
        assertEquals("Maya should lost 1 point of armor", armorBefore - 1, this.maya.getArmor());
    }

}