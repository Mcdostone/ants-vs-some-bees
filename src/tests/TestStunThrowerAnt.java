package tests;

import ants.ShortThrowerAnt;
import ants.StunThrowerAnt;
import core.AntColony;
import core.Bee;

import static org.junit.Assert.assertEquals;

/**
 * Test class for StunThrowerAnt class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class TestStunThrowerAnt extends TestAnt {

    private int mayaArmor = 3;
    private Bee maya;


    public TestStunThrowerAnt() {
        super(new StunThrowerAnt(), 6, 1);
    }


    @org.junit.Before
    /**
     * This method is called before each test method.
     */
    public void setUp() throws Exception {
        super.setUp();
        this.colony =  new AntColony(1, 5, 0, 6);
        this.maya = new Bee(this.mayaArmor);
    }


    @org.junit.Test
    /** Test action method in a normal case */
    public void testAction() {
        int armorBefore = this.maya.getArmor();

        placeAnt(this.cobaye, 0);
        // Distance : 2
        placeBee(this.maya, 2);

        this.cobaye.action(this.colony);
        this.maya.action(this.colony);

        // 6 - foodCost = 0
        assertEquals("The colony should have 0 food", 0, this.colony.getFood());
        assertEquals("The shouldn't be hurt", armorBefore, this.maya.getArmor());
        assertEquals("Maya should stay at the same beginning place", maya.getPlace(), this.colony.getPlaces()[2]);
    }

    public void testActionAfterStun() {
        int armorBefore = this.maya.getArmor();

        placeAnt(this.cobaye, 0);
        // Distance : 2
        placeBee(this.maya, 2);

        this.cobaye.action(this.colony);
        this.maya.action(this.colony);

        // 6 - foodCost = 0
        assertEquals("The colony should have 0 food", 0, this.colony.getFood());
        assertEquals("Maya should stay at the same beginning place", maya.getPlace(), this.colony.getPlaces()[2]);
        assertEquals("The shouldn't be hurt", armorBefore, this.maya.getArmor());

        this.maya.action(this.colony);
        assertEquals("The shouldn't be hurt", armorBefore, this.maya.getArmor());
        assertEquals("Maya should move to the next place", maya.getPlace(), this.colony.getPlaces()[1]);
    }

}