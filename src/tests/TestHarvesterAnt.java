package tests;

import ants.HarvesterAnt;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test class for HarvesterAnt class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class TestHarvesterAnt extends TestAnt {


    public TestHarvesterAnt() {
        super(new HarvesterAnt(), 2, 1);
    }


    @org.junit.Before
    public void setUp() throws Exception {
        super.setUp();
    }


    @org.junit.Test
    /** Test action method in a normal case */
    public void testAction() {
        this.cobaye.action(this.colony);

        assertEquals("The colony should have 1 food", 1, this.colony.getFood());
    }


}