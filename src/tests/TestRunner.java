package tests;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
    public static void main(String[] args) {
        Class[] classTests = {TestHarvesterAnt.class, TestHungryAnt.class, TestWallAnt.class, TestThrowerAnt.class, TestFireAnt.class, TestScubaThrowerAnt.class, TestNinjaAnt.class, TestBodyGuardAnt.class,
                TestQueenAnt.class, TestLongThrowerAnt.class, TestShortThrowerAnt.class, TestStunThrowerAnt.class};

        for(Class c : classTests) {
            Result result = JUnitCore.runClasses(c);
            for (Failure failure : result.getFailures()) {
                System.out.println(failure.toString());
            }
        }
        
        System.out.println("Finished!");
    }
}  	