package tests;

import ants.HungryAnt;
import core.AntColony;
import core.Bee;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for HungryAnt class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class TestHungryAnt extends TestAnt {

    public TestHungryAnt() {
        super(new HungryAnt(), 4, 1);
    }

    @org.junit.Before
    public void setUp() throws Exception {
        // 1 tunnel, tunnel of length 5, moat frequency = 0, food : 6)
        this.colony =  new AntColony(1, 6, 0, 4);
    }


    @Override
    @org.junit.Test
    public void testAction() {
        placeAnt(this.cobaye, 0);
        Bee maya  = new Bee(3);
        placeBee(maya, 0);

        this.cobaye.action(this.colony);
        assertTrue("The bee should be killed", doesInsectIsDead(maya));
        assertEquals("The HungryAnt should'nt suffer damages", this.expectedArmor, this.cobaye.getArmor());
    }


    @org.junit.Test
    /**
     * Test action method when there are several bees on the same
     */
    public void testActionSeveralBees() {
        placeAnt(this.cobaye, 0);
        Bee maya  = new Bee(3);
        Bee maya2  = new Bee(3);
        placeBee(maya, 0);
        placeBee(maya2, 0);

        this.cobaye.action(this.colony);
        assertTrue("the first bee should be killed", doesInsectIsDead(maya));

        maya2.action(this.colony);
        assertTrue("HungryAnt should be killed", doesInsectIsDead(this.cobaye));

    }


    @org.junit.Test
    /**
     * Test action method for the cooldown
     */
    public void testActionCooldown() {
        placeAnt(this.cobaye, 0);
        Bee maya  = new Bee(3);
        Bee maya2  = new Bee(3);
        placeBee(maya, 0);
        placeBee(maya2, 5);

        // Begin
        maya2.action(this.colony);
        this.cobaye.action(this.colony);
        assertTrue("The first bee should be dead", doesInsectIsDead(maya));

        // 1 tour
        maya2.action(this.colony);
        this.cobaye.action(this.colony);

        // 2 tour
        maya2.action(this.colony);
        this.cobaye.action(this.colony);

        // 3 tour
        maya2.action(this.colony);
        this.cobaye.action(this.colony);

        // can attack
        maya2.action(this.colony);
        this.cobaye.action(this.colony);
        assertTrue("The second bee should be killed (the cooldown is over)", doesInsectIsDead(maya2));
    }

    @org.junit.Test
    /**
     * Test action method for the cooldown in te case when this ant is killed because a long cooldown
     */
    public void testActionCooldownTooLong() {
        placeAnt(this.cobaye, 0);
        Bee maya  = new Bee(3);
        Bee maya2  = new Bee(3);
        placeBee(maya, 0);
        placeBee(maya2, 3);

        // Begin
        maya2.action(this.colony);
        this.cobaye.action(this.colony);
        assertTrue("The first bee should be dead", doesInsectIsDead(maya));

        // 1 tour
        maya2.action(this.colony);
        this.cobaye.action(this.colony);

        // 2 tour
        this.cobaye.action(this.colony);
        maya2.action(this.colony);

        this.cobaye.action(this.colony);
        maya2.action(this.colony);

        assertTrue("The hungryAnt should be killed (again in cooldown)", doesInsectIsDead(this.cobaye));
        assertFalse("The second bee shouldn't be killed ", doesInsectIsDead(maya2));
    }



}
