package tests;

import core.*;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Abstract Test class for Ant class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public abstract class TestAnt {

    protected AntColony colony;

    protected Ant cobaye;
    protected int expectedFoodCost;
    protected int expectedArmor;


    public TestAnt(Ant cobaye, int expectedFoodCost, int expectedArmor) {
        this.cobaye = cobaye;
        this.expectedArmor = expectedArmor;
        this.expectedFoodCost = expectedFoodCost;
    }


    /**
     * Place an Ant at the specified place in the tunnel.
     * @param a The ant to place
     * @param index the index in th tunnel
     */
    protected void placeAnt(Ant a, int index) {
        Place[] places = this.colony.getPlaces();
        this.colony.deployAnt(places[index], a);
    }

    /**
     * Place a bee at the specified place in the tunnel.
     * @param b The ant to place
     * @param index the index in th tunnel
     */
    protected void placeBee(Bee b, int index) {
        Place[] places = this.colony.getPlaces();
        places[index].addInsect(b);
    }


    @org.junit.Before
    /**
     * This method is called before each test method.
     */
    public void setUp() throws Exception {
        // 1 tunnel, tunnel of length 5, moat frequency = 0, food : 6)
        this.colony =  new AntColony(1, 5, 0, 0);
    }


    @org.junit.Test
    /** Test the default constructor */
    public void testConstructor() {
        String className = this.cobaye.getClass().getName();
        assertEquals("A " + className  + " should cost 4 food", this.expectedFoodCost, this.cobaye.getFoodCost());
        assertEquals("A " + className + " should have 1 armor", this.expectedArmor, this.cobaye.getArmor());
    }


    @org.junit.Test
    /** Test action method in a normal case */
    public abstract void testAction();


    public boolean doesInsectIsDead(Insect i) {
        return (i.getPlace() == null) && (i.getArmor() <= 0);
    }

    @org.junit.Test

    /**
     * Test if the ant is watersafe.
     */
    public void testWatersafe() {
        assertFalse("The " + this.cobaye.getClass().getName() + " should not bee watersafe", this.cobaye.isWatersafe());
    }

    /**
     * Test if the ant can block bees.
     */
    public void testIsBlockingBee() {
        assertTrue("The " + this.cobaye.getClass().getName() + " should block bees", this.cobaye.isBlockingBee());
    }

}