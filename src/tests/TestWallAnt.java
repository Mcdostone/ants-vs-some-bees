package tests;

import ants.WallAnt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for WallAnt class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class TestWallAnt extends TestAnt{

    public TestWallAnt() {
        super(new WallAnt(), 4, 4);
    }


    @Override
    public void testAction() {
        assertTrue(this.cobaye.isBlockingBee());
    }

}
