package tests;

import ants.NinjaAnt;
import core.AntColony;
import core.Bee;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test class for NinjaAnt class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class TestNinjaAnt extends TestAnt {

    public TestNinjaAnt() {
        super(new NinjaAnt(), 6, 1);
    }


    @org.junit.Before
    /**
     * This method is called before each test method.
     */
    public void setUp() throws Exception {
        super.setUp();
        this.colony =  new AntColony(1, 5, 0, 6);
    }

    @Override
    public void testAction() {
        int armorBee = 3;
        Bee maya = new Bee(armorBee);
        int armorBefore = this.cobaye.getArmor();
        placeAnt(this.cobaye, 3);
        placeBee(maya, 4);

        this.cobaye.action(this.colony);
        maya.action(this.colony);

        // Should hurt bees
        this.cobaye.action(this.colony);
        maya.action(this.colony);

        assertEquals("The bee should be hurt", armorBee - 1, maya.getArmor());
        assertEquals("The NinjaAnt should not be touched", armorBefore, this.cobaye.getArmor());
        assertFalse("The bee should not be dead", doesInsectIsDead(maya));

    }


    @Override
    public void testIsBlockingBee() {
        assertFalse("The " + this.cobaye.getClass().getName() + " should not block bees", this.cobaye.isBlockingBee());
    }

}
