package tests;

import ants.FireAnt;
import core.AntColony;
import core.Bee;
import junit.framework.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for FireAnt class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class TestFireAnt extends TestAnt {

    private final int DAMAGE = 3;

    public TestFireAnt() {
        super(new FireAnt(), 4, 1);
    }

    @org.junit.Before
    public void setUp() throws Exception {
        // 1 tunnel, tunnel of length 5, moat frequency = 0, food : 4)
        this.colony =  new AntColony(1, 6, 0, 4);
    }

    @Override
    @org.junit.Test
    public void testAction() {
        placeAnt(this.cobaye, 0);
        Bee maya  = new Bee(3);
        placeBee(maya, 0);

        maya.action(this.colony);
        this.cobaye.action(this.colony);

        assertTrue("The bee should be killed", doesInsectIsDead(maya));
        assertTrue("The fire Ant should be killed", doesInsectIsDead(this.cobaye));
    }



    @org.junit.Test
    /**
     * Test the action method when the bee has more than 3 armor.
     */
    public void testActionStrongBee() {
        placeAnt(this.cobaye, 0);
        int armor = 10;
        Bee maya  = new Bee(armor);
        placeBee(maya, 0);

        maya.action(this.colony);
        this.cobaye.action(this.colony);

        assertFalse("The bee should'nt be killed", doesInsectIsDead(maya));
        assertEquals("The bee should have 7 points of armor", armor - DAMAGE, maya.getArmor());
        assertTrue("The fire Ant should be killed", doesInsectIsDead(this.cobaye));
    }
}
