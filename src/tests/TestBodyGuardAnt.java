package tests;

import ants.BodyguardAnt;
import ants.HarvesterAnt;
import core.AntColony;
import core.Bee;

import static org.junit.Assert.*;


/**
 * Test class for BodyGuardAnt class.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class TestBodyGuardAnt extends TestAnt {

    private HarvesterAnt cobaye2;

    public TestBodyGuardAnt() {
        super(new BodyguardAnt(), 5, 2);
    }


    @org.junit.Before
    /**
     * This method is called before each test method.
     */
    public void setUp() throws Exception {
        super.setUp();
        // food = cost BodyGuard + cost harvester = 7
        this.colony =  new AntColony(1, 5, 0, 7);
        this.cobaye2 = new HarvesterAnt();

    }

    @Override
    public void testAction() {
        placeAnt(this.cobaye, 2);
        placeAnt(this.cobaye2, 2);
        assertEquals("The tunnel should contain 2 ants", 2, this.colony.getAllAnts().toArray().length);
    }

    @org.junit.Test
    /**
     * Test the case when a bodyGuardAnt protects another ant.
     */
    public void testActionBasicAttack() {
        placeAnt(this.cobaye, 2);
        placeAnt(this.cobaye2, 2);
        int armorHarvester = this.cobaye2.getArmor();
        Bee maya = new Bee(3);
        placeBee(maya,2);

        this.cobaye.action(this.colony);
        this.cobaye2.action(this.colony);
        maya.action(this.colony);

        assertEquals("The tunnel should contain 2 ants", 2, this.colony.getAllAnts().toArray().length);
        assertEquals("The bodyGuardAnt should be hurt", this.expectedArmor - 1, this.cobaye.getArmor());
        assertEquals("The Harvester should give 1 food", 1, this.colony.getFood());
        assertEquals("The Harvester should not be hurt", armorHarvester, this.cobaye2.getArmor());
    }


    @org.junit.Test
    /**
     * Test the case when a bodyGuardAnt protects another ant.
     */
    public void testActionBasicGuardWillDie() {
        placeAnt(this.cobaye, 2);
        placeAnt(this.cobaye2, 2);
        int armorHarvester = this.cobaye2.getArmor();
        Bee maya = new Bee(3);
        placeBee(maya,2);


        this.cobaye.action(this.colony);
        this.cobaye2.action(this.colony);
        maya.action(this.colony); // bodyGuard has 1 armor


        this.cobaye.action(this.colony);
        this.cobaye2.action(this.colony);
        maya.action(this.colony); // bodyGuard is dead

        assertTrue("The bodyGuard should be dead", doesInsectIsDead(this.cobaye));
        assertFalse("The harvester should not be dead", doesInsectIsDead(this.cobaye2));
        assertEquals("The tunnel should contain 1 ant", 1, this.colony.getAllAnts().toArray().length);
        assertEquals("The ant at the place 2 should be the harvester", this.cobaye2, this.colony.getPlaces()[2].getAnt());
    }


}