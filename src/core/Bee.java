package core;

/**
 * Represents a Bee;
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class Bee extends Insect {

	private static final int DAMAGE = 1;
	protected int isStuned = 0;

	/**
	 * Creates a new bee with the given armor
	 * @param armor The bee's armor
	 */
	public Bee (int armor) {
		super(armor);
		// Bees are all watersafe.
		watersafe = true;
	}

	/**
	 * Deals damage to the given ant
	 * @param ant The ant to sting
	 */
	public void sting (Ant ant) {
		ant.reduceArmor(DAMAGE);
	}

	/**
	 * Moves to the given place
	 * @param place The place to move to
	 */
	public void moveTo (Place place) {
		this.place.removeInsect(this);
		place.addInsect(this);
	}

	@Override
	public void leavePlace () {
		place.removeInsect(this);
	}

	/**
	 * @return true if the bee cannot advance (because an ant is in the way)
	 */
	public boolean isBlocked () {
		if(place != null  && place.getAnt() != null)
			return place.getAnt().isBlockingBee();
		else
			return false;
	}

	/**
	 * A bee's action is to sting the Ant that blocks its exit if it is blocked,
	 * otherwise it moves to the exit of its current place.
	 */
	@Override
	public void action (AntColony colony) {
		if(this.isStuned()) {
			this.isStuned--;
		}
		else {
			if (isBlocked()) {
				sting(place.getAnt());
			}
			else if((armor > 0) )
				moveTo(this.place.getExit());
		}
	}

	public boolean isStuned() {
		return this.isStuned > 0;
	}

	public void stun(int tours) {
		this.isStuned = tours;
	}
}
