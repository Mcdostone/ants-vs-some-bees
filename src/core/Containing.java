package core;

/**
 * Interface Containing enables to have several ants on the same place.
 * This interface applies on an ant.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public interface Containing {

    /**
     * Add a insect into the container Ant
     * @param insect Insect to add
     * @return {@code true} if the insect has been added.
     */
	public boolean addContainedInsect(Insect insect);

    /**
     * Remove a insect into the container Ant
     * @return {@code true} if the insect has been removed.
     */
	public boolean removeContainedInsect();

    /**
     * @return Get the contained Insect.
     */
	public Insect getContainedInsect();
}
