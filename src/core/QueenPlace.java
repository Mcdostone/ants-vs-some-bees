package core;


/**
 * The special place for the Queen ant.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class QueenPlace extends Place {
	private Place realQueenPlace;

	public QueenPlace(String name, Place exit) {
		super(name, exit);
		realQueenPlace = null;
	}

	public QueenPlace(String name) {
		super(name);
		realQueenPlace = null;
	}


    private static Bee[] concatArrays(Bee[] a1, Bee[] a2) {
        Bee[] both = new Bee[a1.length + a2.length];

        int index = 0;
        for (Bee b : a1) {
            both[index] = b;
            index++;
        }
        for (Bee b : a2) {
            both[index] = b;
            index++;
        }

        return both;
    }

	public Bee[] getBees() {
        if(this.realQueenPlace != null)
            return concatArrays( this.bees.toArray(new Bee[0]), this.realQueenPlace.getBees());
        else
            return this.bees.toArray(new Bee[0]);
	}

	
	public void setRealQueenPlace(Place p){
		realQueenPlace = p;
	}
}
