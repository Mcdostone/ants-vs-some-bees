package core;

/**
 * Water represents a new king of place filled with water.
 * Bees can move under water but not all ants.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public class Water extends Place {

	/**
	 * First constructor
	 * @param name Name of the place
	 * @param exit Place to go when an insect leaves it.
     */
	public Water(String name, Place exit){
		super(name, exit);
	}

	public Water(String name){
		super(name);
	}
}
