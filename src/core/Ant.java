package core;

/**
 * A class representing a basic Ant.
 *
 * @author Eliot Godard
 * @author Yann Prono
 */
public abstract class Ant extends Insect {

	// the amount of food needed to make this ant
	protected int foodCost;

    // Is this can block bees in the tunnel ?
	protected boolean blockingBee = true;

	/**
	 * Creates a new Ant, with a food cost of 0.
	 * @param armor The armor of the ant.
	 */
	public Ant (int armor) {
		super(armor, null);
		this.foodCost = 0;
	}

    /**
     * Second constructor which specifies food cost and armor of the ant.
     *
     * @param foodCost Amount of food needed to make this ant.
     * @param armor Armor of this ant.
     */
	public Ant (int foodCost, int armor) {
		super(armor, null);
		this.foodCost = foodCost;
	}

	/**
	 * @return the ant's good cost
	 */
	public int getFoodCost () {
		return foodCost;
	}

	/**
	 * @return if the ant is blocking bees
	 */
	public boolean isBlockingBee() {
		return blockingBee;
	}

	/**
	 * Removes the ant from its current place
	 */
	@Override
	public void leavePlace () {
		place.removeInsect(this);
	}
}
