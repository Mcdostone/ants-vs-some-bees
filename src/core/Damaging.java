package core;

public interface Damaging {
	public void galvanise(boolean b);

	public int getDamage();
}
